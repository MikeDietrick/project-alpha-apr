from django.shortcuts import render

from projects.models import Project

# Create your views here.


def list_project(request):
    name = Project.objects.all()
    description = Project.objects.all()
    context = {
        "list_project": (name, description),
    }
    return render(request, "projects/list.html", context)
